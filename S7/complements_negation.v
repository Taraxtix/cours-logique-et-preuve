(* Consigne générale: structurer proprement vos scripts de preuve;
   chaque fois qu'une tactique produit plus d'un but, utiliser des
   puces pour séparer les preuves des deux buts.

   Vous pouvez aussi essayer, autant que possible, d'enchaîner les
   tactiques au moyen de points-virgules.
*)

(* Tous les "Admitted" doivent être remplacés par vos scripts de preuves. *)

(* Quelques lemmes à prouver *)
Section Complements_Negation.
Variables P Q R S: Prop.

Lemma contraposee: (P->Q) -> (~Q->~P).
Proof.
   intros P_Q NQ p.
   exfalso; apply NQ.
   apply P_Q; assumption.
Qed.

Lemma autre_contraposee: (P->~Q) -> (Q->~P).
Proof.
   intros P_NQ q p.
   exfalso; apply P_NQ.
   all: assumption.
Qed.

Lemma DS1: ((P->Q)->R)->((~P->~Q)->~R)->(P->Q->S).
Proof.
   intros P_Q'_R NP_NQ'_NR p q.
   exfalso; apply NP_NQ'_NR.
   -  intro NP.
      exfalso; apply NP; assumption.
   -  apply P_Q'_R.
      intro p'; assumption.
Qed.

Lemma DS2: (P->Q)->~(R->Q)->(~R->S) -> (R->P->S).
Proof.
   intros P_Q N'R_Q NR_S r p.
   exfalso; apply N'R_Q.
   intro r'.
   apply P_Q; assumption.
Qed.

(* Ne pas foncer sans réfléchir *)
Lemma piege: (P->~Q)->(~Q->R)->(R->~S)->(~S->~P) -> ~P.
Proof.
   intros P_NQ NQ_R R_NS NS_NP p.
   exfalso; apply NS_NP.
   -  apply R_NS.
      apply NQ_R.
      apply P_NQ; assumption.
- assumption.
Qed.

End Complements_Negation.

Section Double_Negation.
(* Autour de la double négation *)
Variables P Q: Prop.
(* De P, on peut déduire non-non-P *)
Lemma dn_i: P-> ~~P.
Proof.
   intros p NP.
   exfalso; apply NP; assumption.
Qed.

(* On a prouvé dans absurd' (fichier negation.v), que, de "non-P implique P",
   on peut déduire, peut-être pas P, mais au moins non-non-P *)

(* Si on suppose, ce qui est plus fort, qu'on peut déduire P
   de "non-P implique P", alors on peut justifier l'élimination de
   la double négation [pour P] *)
Lemma trop_de_negations: ((~P->P)->P) -> (~~P -> P).
Proof.
   intros NP_P'_P NNP.
   apply NP_P'_P.
   intro NP.
   exfalso; apply NNP; assumption.
Qed.

End Double_Negation.


Section Preuves_de_sequents.
Variables P Q R S: Prop.
(* Les exercices restants consistent à formuler soi-même les lemmes:
   chaque séquent à prouver va constituer une sous-section, avec ses
   hypothèses *)

(* Et bien sûr, faire la preuve ensuite! *)


(* Exemple *)
  Section Exemple.
  (* Séquent à prouver: P->Q, P->~Q |- P->R *)
  Hypothesis H: P->Q.
  Hypothesis H1: P->~Q.
  Lemma exemple: P->R.
  Proof.
  intro p. exfalso.
  apply H1.
  - assumption.
  - apply H. assumption.
  Qed.
  End Exemple.

  Section Sequent1.
  (* Séquent à prouver: 
     (P->Q)->~Q |- (P->Q)->~P
  *)
Hypothesis H0: (P->Q)->~Q.
Lemma sequent1: (P->Q)->~P.
Proof.
   intros P_Q p.
   exfalso; apply H0.
   - assumption.
   - apply P_Q; assumption.
Qed. 
  End Sequent1.

  Section Sequent2.
  (* Séquent à prouver:
     (P->Q)->R, (P->Q)->~R |- Q->S
  *)
Hypothesis H0: (P->Q)->R.
Hypothesis H1: (P->Q)->~R.
Lemma sequent2: Q->S.
Proof.
   intros q.
   assert (P_Q: P->Q).
   {
      intro; assumption.
   }
   exfalso; apply H1.
   -  assumption.
   -  apply H0; assumption.
Qed.
  
  End Sequent2.

  Section Sequent3.
  (* Séquent à prouver:
     P->Q, ~P->Q |- ~~Q
  *)
Hypothesis H0: P->Q.
Hypothesis H1: ~P->Q.
Lemma sequent3: ~~Q.
Proof.
   intros NQ.
   apply NQ.
   apply H1.
   intro p.
   apply NQ.
   apply H0; assumption.
Qed.
  
  End Sequent3.

End Preuves_de_sequents.