Require Import Setoid Classical.

(*  Logique classique
    On peut sauter les 2 commandes suivantes 
 *)

(* un peu de magie noire *)
Definition EXM :=   forall A:Prop, A \/ ~A.

Ltac add_exm  A :=
  let hname := fresh "exm" in
  assert(hname : A \/ ~A);[classical_right; assumption|].


Section LK.
  (* 
   Pour ajouter une instance du tiers-exclu de la forme  A \/ ~A 
   il suffit d'exécuter la tactique "add_exm A"
   *)

  Variables P Q R S T : Prop.

  Lemma double_neg :forall P:Prop, ~~ P -> P.
  Proof.
    intros p H.
    add_exm  p. (* "je fais un tiers exclus sur P " *)
    destruct exm. (* Presque toujours, destruct suit add_exm *)
    - assumption.
    - assert (f:False).
      {
        apply H; assumption.
      }
      destruct f. (* ou: exfalso, etc. *)
   Qed.

  (* Variantes: tactiques classical_left et classical_right.
     Pour prouver A \/ B:
     - classical_left demande de prouver A, avec ~B comme hypothèse en plus.
     - classigal_right demande de prouver B, avec ~A comme hypothèse en plus.
  *)

  Lemma weak_exm: P \/ (P->Q).
  Proof.
  classical_right.
  intro p; exfalso; apply H; assumption.
  Qed.

  (* Exercice: completer toutes les preuves, en remplaçant les
     "Admitted" par des preuves terminées par "Qed."; et 
     sans utiliser ni auto, ni tauto.  *)

  Lemma de_morgan : ~ ( P /\ Q) <-> ~P \/ ~Q.
  Proof.
    split.
      - intro n_PandQ.
       classical_left.
       add_exm P.
       destruct exm as [p | nP].
        + intro.
          apply n_PandQ.
          split.
            * assumption.
            * apply double_neg; assumption.
        + assumption.
      - intro nPornQ.
        destruct nPornQ as [H|H].
          all: 
            intro;
            apply H;
            destruct H0 as [p q]; assumption.
  Qed.

  Lemma not_impl_and : ~(P -> Q) <-> P /\ ~ Q.
  Proof.
    split.
      - intro nPimplQ.
        split.
          + add_exm P; destruct exm as [p | nP].
              * assumption.
              * destruct nPimplQ.
                intro p.
                destruct nP; assumption.
          + intro q.
            destruct nPimplQ.
            intro p.
            assumption.
      - intros PandnQ PimplQ.
        destruct PandnQ as [p nQ].
        destruct nQ.
        apply PimplQ; assumption.
  Qed.

  Lemma contraposee: (P -> Q) <-> (~Q -> ~P).
  Proof.
    split.
      - intros PimplQ nQ p.
        destruct nQ.
        apply PimplQ; assumption.
      - intros nQimplnP p.
        add_exm Q; destruct exm as [q | nQ].
          + assumption.
          + destruct nQimplnP.
            all: assumption.      
  Qed.

  Lemma exm_e : (P -> Q) -> (~P -> Q) -> Q.
  Proof.
    intros PimplQ nPimplQ.
    add_exm P; destruct exm as [p | nP].
      all: (apply nPimplQ; assumption) || (apply PimplQ; assumption). 
  Qed.

  Lemma exo_16 : (~ P -> P) -> P.
  Proof.
    intro nPimplP.
    add_exm P; destruct exm as [p | nP].
      all: assumption || (apply nPimplP; assumption).
  Qed.

  Lemma double_impl : (P -> Q) \/ (Q -> P).
  Proof.
    add_exm Q; destruct exm as [q | nQ].
    - left; intro; assumption.
    - right; intro; destruct nQ; assumption.
  Qed.

  Lemma imp_translation : (P -> Q) <-> ~P \/ Q.
  Proof.
    split.
      - intro PimplQ.
        add_exm P; destruct exm as [p | nP].
          + right; apply PimplQ; assumption.
          + left; assumption.
      - intros nPorQ p.
        destruct nPorQ as [nP | q].
          + destruct nP; assumption.
          + assumption.
  Qed.

  Lemma Peirce : (( P -> Q) -> P) -> P.
  Proof.
    intro PimplQ_implP.
    add_exm P; destruct exm as [p | nP].
      - assumption.
      - apply PimplQ_implP.
        intro p.
        destruct nP; assumption.
  Qed.

  (* Quelques exercices d'anciens tests *) 
  Lemma test_1: (P->Q)->(~P->R)->(R->Q)->Q.
  Proof.
    intros PimplQ nPimplR RimplQ.
    add_exm P; destruct exm as [p | nP].
    - apply PimplQ; assumption.
    - apply RimplQ; apply nPimplR; assumption.
  Qed.

  Lemma test__2: (P \/ (Q\/R))-> (~P) -> (~R) -> (P\/Q).
  Proof.
    intros PorQorR nP nR.
    destruct PorQorR as [p | QorR].
    - destruct nP; assumption.
    - destruct QorR as [q | r].
      + right; assumption.
      + destruct nR; assumption.
  Qed.

  Lemma test_3: (~P-> Q/\R)->(Q->~R)->P.
  Proof.
    intros nPimpl_QandR QimplnR.
    add_exm P; destruct exm as [p | nP].
      - assumption.
      - assert (QandR : Q /\ R). { apply nPimpl_QandR; assumption. }
        destruct QandR as [q r].
        destruct QimplnR; assumption.
  Qed.

  Lemma test_4: (~P->Q)->(~Q\/R)->(P->R)->R.
  Proof.
    intros nPimplQ nQorR PimplR.
    add_exm P; destruct exm as [p | nP].
      - apply PimplR; assumption.
      - destruct nQorR as [nQ | r].
        all: try (destruct nQ; apply nPimplQ); assumption.
  Qed.

(* Set Printing Parentheses. *)
  Lemma test_5: (P->Q)->(~P->~Q)->((P/\Q) \/ ~(P\/Q)).
  Proof.
      intros PimplQ nPimplnQ.
      add_exm P; destruct exm as [p | nP].
      - left.
        split.
          all: try apply PimplQ; assumption.
      - right.
        intros PorQ.
        destruct PorQ as [p | q].
        + destruct nP; assumption.
        + destruct nPimplnQ.
          all: assumption.
  Qed.

  Lemma test_6: (P->Q)->(~P->Q)->(Q->R)->R.
  Proof.
    intros PimplQ nPimplQ QimplR.
    add_exm P; destruct exm as [p | nP].
    all: apply QimplR.
    - apply PimplQ; assumption.
    - apply nPimplQ; assumption.                                                                                                                                                                                                                                                                                                
  Qed.

End LK.

Section Club_Ecossais. (* version propositionnelle *)
  Variables E R D M K: Prop.
  (* Ecossais, chaussettes Rouges, sort le Dimanche, Marié, Kilt *)

  Hypothesis h1: ~E -> R.
  (* Tout membre non ecossais porte des chaussettes rouges *)
  Hypothesis h2: M -> ~D.
  (* Les membres maries ne sortent pas le dimanche *)
  Hypothesis h3: D <-> E.
  (* Un membre sort le dimanche si et seulement si il est ecossais *)
  Hypothesis h4: K -> E /\ M.
  (* Tout membre qui porte un kilt est ecossais et est marie *)
  Hypothesis h5: R -> K.
  (* Tout membre qui porte des chaussettes rouges porte un kilt *)
  Hypothesis h6: E -> K.
  (* Tout membre ecossais porte un kilt. *)

  Lemma personne: False. (* Le club est vide! *)
  Proof.
    assert (EandM: E /\ M). {
      apply h4.
      add_exm E; destruct exm as [e | nE].
      + apply h6; assumption.
      + apply h5; apply h1; assumption.
    }
    destruct EandM as [e m].
    destruct h2.
    all: try rewrite h3; assumption.
  Qed.

End Club_Ecossais.  
  
(** On peut sauter cette section *)

(* Au sens strict, cette partie est hors programme; il s'agit de voir que 
   diverses hypothèses (toutes formulées "au second ordre": avec des 
   quantificateurs universels sur des propositions)
   sont équivalentes, et correspondent à la logique classique *)
(* ATTENTION: pour que ces exercices aient un sens, il faut les faire SANS
   utiliser les tactiques réservées à la logique classique (add_exm, ou
   classical_left, ou classical_right *)
Section Second_ordre. 
  Definition PEIRCE := forall A B:Prop, ((A -> B) -> A) -> A.
  Definition DNEG := forall A, ~~A <-> A.
  Definition IMP2OR := forall A B:Prop, (A->B) <-> ~A \/ B.

  Lemma L2 : IMP2OR -> EXM.
  Proof.
    unfold IMP2OR, EXM.
    intros.
    assert (~ A \/ A). {
      rewrite <- H. (* Coq "voit" qu'il suffit de prendre B=A; il va falloir prouver A->A *)
      intro; assumption.
    }
    destruct H0.
    all: (right; assumption) || (left; assumption).
  Qed.
  

  Lemma L3 : EXM -> DNEG.
  Proof.
    unfold DNEG , EXM.
    intros. (* H permet de faire un tiers exclus sur A *)
    assert (H0: A \/ ~A). { apply H. }
    assert (doubleNeg: ~~A <-> A). {
      split.
      - intro nnA.
        destruct H0 as [a | nA].
        + assumption.
        + destruct nnA; assumption.
      - intros a nA.
        apply nA; assumption.
    }
    destruct H0.
    all: apply doubleNeg.
  Qed.

  Lemma L4 : PEIRCE -> DNEG.
  Proof.
    unfold DNEG, PEIRCE.
    intros.
    split.
    - intro nnA.
      apply H with False .
      intro nA.
      destruct nnA; assumption.
    - intros a nA.
      destruct nA; assumption.
  Qed.
  
  Lemma L5 : EXM -> PEIRCE.
  Proof.
    unfold PEIRCE, EXM.
    intros.
    assert (H1: A \/ ~A). { apply H. }
    destruct H1 as [a | nA].
    - assumption.
    - apply H0.
      intro a.
      destruct nA; assumption.
  Qed.

End Second_ordre.
