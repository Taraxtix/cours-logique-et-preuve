Section Negation.
  Variables P Q R S T: Prop.

  (* unfold not: expansion de la négation dans le but *)
  (* unfold not in X: expansion de la négation dans l'hypothèse X *)
  (* exfalso: transforme le but courant en False; c'est l'équivalent
     de la règle d'élimination de la contradiction *)

  (* Executez cette preuve en essayant de comprendre le sens de chacune des nouvelles tactiques utilisées. *)
  Lemma absurde_exemple: P -> ~P -> S.
  Proof.
    intros p np.
    unfold not in np.
    exfalso.
    apply np.
    assumption.
  Qed.
  
  Lemma triple_neg_e : ~~~P -> ~P.
  Proof.
     (* intro H. 
     intro H0.
     apply H.
     intro H1.
     apply H1; assumption.
   Restart.  Annule la preuve en cours, et en commence un autre *)
   unfold not.
   auto.
   (* auto est une tactique qui est capable de beaucoup, mais qu'on
      s'interdira d'utiliser dans nos preuves *)
   Qed.

  (* Début des exercices *)

  (* QUESTION: Remplacer les Admitted par des scripts de preuve *)
  Lemma absurde: (P -> Q) -> (P -> ~Q) -> (P -> S).
  Proof.
    intros P_Q P_nQ p.
    unfold not in P_nQ.
    exfalso.
    apply P_nQ.
    - assumption.
    - apply P_Q; assumption.
  Qed.

  Lemma triple_abs: ~P -> ~~~P.
  Proof.
    intro nP.
    unfold not.
    unfold not in nP.
    intro P_F_F.
    apply P_F_F.
    assumption.
  Qed.
  
  Lemma absurd' : (~P -> P) -> ~~P.
  Proof.
    intro nP_P.
    unfold not at 1.
    intro nP.
    assert (p: P). {
      apply nP_P; assumption.
    }
    apply nP; assumption.
  Qed.

  Definition Peirce  := ((P -> Q) -> P) -> P.

  (* On va prouver non-non-Peirce *)
  Lemma Peirce_2 : ~~ Peirce.
  Proof.
    (* Strategie: sous hypothese ~Peirce [par intro], montrer ~P, puis s'en 
       servir pour montrer Peirce, et on aura une contradiction
       entre Peirce et ~Peirce *)
    intro.
    assert (nP: ~P). {
      unfold not in H.
      unfold Peirce in H.
      exfalso.
      apply H.
      intro P_Q_P.
      apply P_Q_P.
      intro p.
      exfalso.
      apply H.
      intro P_Q_P'; assumption.
    }
    unfold not in H.
    apply H.
    unfold Peirce.
    intro P_Q_P.
    unfold not in nP.
    exfalso.
    apply nP.
    apply P_Q_P.
    intro p.
    exfalso.
    apply nP; assumption.
  Qed. (* À vous de finir *)

  (* Une série de séquents à prouver; à chaque fois, il faut
  l'énoncer, en introduisant les hypothèses au moyen d'une
  sous-section. *)

  (* P->Q, R->~Q, P->R |- P->S *)
  Section S1.
  Hypothesis H0: P->Q.
  Hypothesis H1: R->~Q.
  Hypothesis H2: P->R.

  Lemma L1: P->S.
  Proof.
    intro p.
    unfold not in H1.
    exfalso.
    apply H1.
    - apply H2; assumption.
    - apply H0; assumption.
  Qed.
  End S1.
  (* ~P->~Q |- ~~Q->~~P *)
  Section S2.
  Hypothesis H: ~P->~Q.

  Lemma L2: ~~Q->~~P.
  Proof.
    intro nnQ.
    unfold not at 1.
    intro nP.
    unfold not in nnQ at 1.
    apply nnQ.
    apply H; assumption.
  Qed.
  End S2.
  (* P->~P |- ~P *)
  Section S3.
  Hypothesis H: P->~P.

  Lemma L3: ~P.
  Proof.
    intro p.
    apply H.
    all:assumption.
  Qed.
  End S3.
  (* ~~P |- ~P->~Q *)
  Section S4.
  Hypothesis H: ~~P.

  Lemma L4: ~P->~Q.
    intro nP.
    unfold not in H at 1.
    exfalso.
    apply H; assumption.
  Qed.
  End S4.
  (* P->~Q, R->Q |- P->~R *)
  Section S5.
  Hypothesis H0: P->~Q.
  Hypothesis H1: R->Q.

  Lemma L5: P->~R.
  Proof.
    intro p.
    intro r.
    apply H0.
    - assumption.
    - apply H1; assumption.
  Qed.
  End S5.
  (* ~(P->Q) |- ~Q *)
  Section S6.
  Hypothesis H: ~(P->Q).

  Lemma L6: ~Q.
  Proof.
    intro q.
    apply H.
    intro; assumption.
  Qed.
  End S6.

  (* Séquents proposés en test par le passé *)

  Section Test01.
    
    Hypothesis H: P->Q.

    Lemma Ex01: ~(~Q->~P) -> R.
    Proof.
      intro nnQ_nP.
      exfalso.
      apply nnQ_nP.
      intro nQ.
      intro p.
      apply nQ.
      apply H; assumption.
    Qed.
  End Test01.

  Section Test02.
    Hypothesis H: ~(P->R).

    Lemma Ex02: Q->(P->Q->R)->P.
    Proof.
      intros q P_Q_R.
      exfalso.
      apply H.
      intro p.
      apply P_Q_R.
      all:assumption.
    Qed.
  End Test02.

  Section Test03.
    Hypothesis H: ~(Q->R).

    Lemma Ex03: (P->Q->R)->(P->Q).
    Proof.
      intros P_Q_R p.
      exfalso.
      apply H.
      intro q.
      apply P_Q_R.
      all:assumption.
    Qed.
  End Test03.

  Section Test04.
    Hypothesis H: ~~P.

    Lemma Ex04: Q->(P->Q->False)->P.
    Proof.
      intros q P_Q_F.
      exfalso.
      apply H.
      intro p.
      apply P_Q_F.
      all:assumption.
    Qed.
  End Test04.
    
End Negation.


